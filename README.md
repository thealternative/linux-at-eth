# Linux at ETH
This guide should help in using Linux for the daily ETH life. It is still work in progress...

## ToDos
- VPN totp
- eth wifi deprecated?

## ETH Network

Use one of these Networks:

- eth
- eth-5
- eduroam
- eduroam-5

Necessary Settings if you are using a GUI

- Security: `WPA/WPA2 Enterprise`
- Authentication: `Protected EAP (PEAP)
- CA certificate: no certificate required, use system certificate or use the [offical eduroam certificate](eduroam.rem)
    - if system certificate, Domain: ethz.ch
- Inner authentication: `MSCHAPv2`
- Username: your student-net or staff-net username (`<eth_username>@student-net.ethz.ch`)
- Password: your student-net or staff-net password

## VPN
ETH provides you with a VPN that allows you to access resources (such as papers, springer link or activating a licence for a program) from outside the ETH Wi-Fi. 
The easiest way to use this VPN on Linux is with the OpenConnect client, which is compatible with the CISCO AnyConnect SSL VPN protocol used by ETH. 

### Installation
First, the client has to be installed. For this run the terminal command listed in your respective section.

#### Ubuntu/Debian and their derivatives (e.g. Mint and similar):
`sudo apt install network-manager-openconnect-gnome`

#### Fedora:
on fedora this should come pre-installed but in case it isn't run\
`sudo dnf install NetworkManager-openconnect-gnome`

#### Arch:
assuming, that you are using NetworkManager use

`sudo pacman -Syu networkmanager-openconnect`

Otherwise, you can find instructions under [this](https://wiki.archlinux.org/title/OpenVPN) link. 

### Setup (Graphical)
On most desktop environments (Gnome, KDE Plasma, Cinnamon, ...) this should work fine.

After installing OpenConnect, go to your desktop manager's network settings and try to find a section called VPN or even just connections.
Add a connection / VPN. The type of connection should state something like `Multi-protocol VPN client` or `Cisco AnyConnect compatible VPN (OpenConnect)`. 
Once you're faced with configuration option enter `sslvpn.ethz.ch` under the `Gateway` field. You can safely ignore 
the other options.
Now you can connect to your VPN using your desktop environments interface. 
When prompted to input your username and password, depending on whether you're a student or staff give `yourname@student-net.ethz.ch` or `yourname@staff-net.ethz.ch` this is the exact same information as is required for your Wi-Fi login. In particular your password will differ from the one used to log into ETH websites.


### Setup (CLI)
If the above instructions did not work for you, you can try this here.

To connect to the VPN type the following in a terminal

`sudo openconnect --useragent=AnyConnect https://sslvpn.ethz.ch`

OpenConnect has to run as root in order to configure local networking.
Next you will be asked to which group you belong to, for which you type `student-net` if you are a student or `staff-net` if you are an employee.
Then you have to enter your ETH username followed by your ETH network password. The username is in the form of `username@student-net.ethz.ch` or `username@staff-net.ethz.ch`.

Alternatively, you can also enter the following command to do all of the above steps (excluding entering the password) in one step:

`openconnect --useragent=AnyConnect sslvpn.ethz.ch -g student-net -u $USER@student-net.ethz.ch`

## Mail

[ETH Help Guide](https://www.isg.inf.ethz.ch/Main/HelpMailClientSetup)

### Thunderbird

Settings:

- Username: <eth username>@ethz.ch
- Password: <eth password>
- Incoming: IMAP, outlook.office365.com, 993, SSL/TLS, OAuth2
- Outgoing: SMTP, outlook.office365.com, 587, STARTTLS, OAuth2

## SSH
Some computers at ETH can be accessed remotely via a software called SSH as long as you are either connected to the VPN or at the university itself.
Using it is pretty straightforward. As most programs on Linux, it can be installed with your package manager. Here is the command for Ubuntu:

`sudo apt install openssh`

Once everything is installed, the command is used in the following way:

`ssh username@target`

Here, username is your usual ETH username and target is either the IP-address (it looks something like `192.168.0.1`) or a domain (for example, `euler.ethz.ch`, or `slab1.ethz.ch`). If you need to start graphical programs on the other computer, you also need to add the `-Y` flag like this: `ssh -Y username@target`.

So, if you were for example to access the Euler supercomputer, here's the command you can use:

`ssh my_eth_username@euler.ethz.ch`

Or if you were for example to access your ETH computer account with all your files remotely, here's the command you can use:

`ssh my_eth_username@slab1.ethz.ch`

### SCP

`scp` stands for "secure copy" and is a utility to copy files between your local and a remote machine, to which you have SSH access. It works pretty similar to how `cp` is used locally.

scp commands look like this:

`scp source destination`

If either source or destination are on the remote machine, they are specified in the following way

`username@address:path`

Otherwise, if they are on your local computer, you can specify them just way you would if you would use `cp` by typing the path.

Here a few examples:

`scp ~/Documents/programming/exercise3.cpp my_eth_username@euler.ethz.ch:~/simulations/`

`scp my_eth_username@euler.ethz.ch:~/simulations/output.png ~/Images/`

## RSYNC

rsync works similar to scp.

`rsync <flags> source destination`

If either source or destination are on the remote machine, they are specified in the following way

`username@address:path`

Useful flags are:

- r: recursive
- a: archival, conserves symlinks, modification times, groups, etc...
- n: dry-run, dont actually do anything
- v: verbose
- z: adds compression
- P: partial and progressive, useful if rsync might stop while copying

## ETH Software Store
The [ETH Software Store](https://itshop.ethz.ch) can be used to easily order and manage software products that you might need to use. 

### Ordering Software

![Software Store](ETH_Software_Shop/1.png)
![Software Store](ETH_Software_Shop/2.png)
![Software Store](ETH_Software_Shop/3.png)
![Software Store](ETH_Software_Shop/4.png)
![Software Store](ETH_Software_Shop/5.png)
![Software Store](ETH_Software_Shop/6.png)
![Software Store](ETH_Software_Shop/7.png)

### Usage 
Once logged in to the service, you can use the Software and Licence tab to order any relevant software. If your stuck at any point there is a folder containing a full visual guide. 

As soon as your licence is granted you'll be able to download the selected software. The installation files are provided to you as a SMB mount.  The mount can only be accessed when at ETH or with a VPN.

Use the following command to mount it to a folder of your choice:

`mount //software.ethz.ch/my_eth_username$ destination_folder -t cifs -o domain=d,username=my_eth_username,vers=2.1`

If this command fails it might be due to the current user not being able to create CIFS mounts. This can be avoided with sudo.

The previous command requires your ETHZ password.


## MATLAB
MATLAB is a programming language and numerical computing plattform. A good alternative to it is GNU Octave, which is a drop-in replacement for many matlab scripts.

### Setup

Installing MATLAB requires you already know how to use the ETH Software Store. If your confused see the section above. 

Once you have obtained a copy use the following commands to install:

`tar -xf R2023a_Update_5_Linux.iso`

This extracts the files from the iso.

`chmod u+x install`

Gives install file executable privileges.

`./install`

Executes the installation script.

> If this step fails you want to delete/rename the file `./bin/glnxa64/libfreetype.so.6` and make sure you have libfreetype
> installed on your system. You may have to repeat this step after installation to fix crashes when trying to open/use the
> built-in text editor.

If you want the installer to use the default install location use sudo. 

The rest of the install script is graphical in nature and is easy to follow therefore it has been omitted for brevity. 

### Troubleshooting

Have a look at [this](https://wiki.archlinux.org/title/MATLAB) Arch wiki entry.

## EULER

### Setup

To use the EULER supercomputer you need to be connected to an ETH network directly or via VPN. To use a remote shell use ssh (see -> SSH). One can ssh into EULER with:

`my_eth_username@euler.ethz.ch`

To copy files from your local computer to EULER or vice versa, scp is recommended. 

### Development
On EULER available text editors are: vi, vim, emacs, nano, ...

To use certain programming tools, so-called "modules" are used. For example, gcc or matlab are such modules.

Load a module: `module load <module>`

List available modules: `module avail`

List loaded modules: `module list`

Unload module: `module unload <module>`

### Job submission
EULER has two different types of nodes: Login nodes and compute nodes. Login nodes are the "entry points" for ssh access and allow you to submit jobs and do small preparations, while compute nodes are there for actually handling the heavy computational lifting (if you use too much computational resources on a login node, your program will be terminated without any warning!). To submit a program to be run on the large compute nodes, the batch system is used.

`bsub <command> [arguments]`

Examples on how to use the batch system can be found here: [Batch System](https://scicomp.ethz.ch/wiki/Using_the_batch_system)

### More information
[Scicomp Wiki Euler](https://scicomp.ethz.ch/wiki/Euler)

[Scicomp User Documentation](https://scicomp.ethz.ch/wiki/User_documentation)

## MFA with KeePassXC

Pictures in the **KeePassXC_OTP** folder

1) Install KeePassXC
2) Set up a new password database (if you do not have one)
    ![KP](KeePassXC_OTP/01_Datenbank1.JPG "kp")
    ![KP](KeePassXC_OTP/02_Datenbank2.jpg "kp")
    ![KP](KeePassXC_OTP/03_Datenbank3.jpg "kp")
3) Create a new entry for ETHZ
    ![KP](KeePassXC_OTP/04_Eintrag1.JPG "kp")
4) Open this link: https://www.password.ethz.ch/qrcode
5) Log in: ETH username and password for web applications, AAI (LDAP)
6) Your so-called personal shared secret and the QR code will now be displayed. Click "Copy to Clipboard" to copy the code.
7) Click in KeePassXC under the menu item "Entries" on "TOTP" and then on the option "Setup TOTP". 
    ![KP](KeePassXC_OTP/05_TOTP-einrichten.JPG "kp")
8) Now paste the copied code into the "Secret Key" field and then click OK
    ![KP](KeePassXC_OTP/06_TOTP-einrichten2.JPG "kp")
9) You can now select the "Show TOTP" option under "TOTP" in the "Entries" menu
    ![KP](KeePassXC_OTP/07_TOTP-anzeigen1.JPG "kp")
10) The OTP code is valid for the displayed period (max. 30 seconds) and must be entered in the input field within this period of validity. To do this, click on "Copy".
    ![KP](KeePassXC_OTP/08_TOTP-anzeigen2.JPG "kp")
11) Verify the functionality by copying the code on the website in the "VALIDATE AND TEST" field 
    ![KP](KeePassXC_OTP/09_TOTP-kopieren1.JPG "kp")

[ETH Knowledge base](https://unlimited.ethz.ch/display/itkb/Multifactor-Authentication)

![KP](KeePassXC_OTP/01_Datenbank1.JPG "kp")



When using Firefox, a Plugin can be installed 
